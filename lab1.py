
from xmlrpc.client import Boolean
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib import colors
import pandas as pd
import os 
#import pathlib as path
#TEST LINE 9
#map_path = path.Path("/gl-latlong-1km-landcover.bsq")
#map_numpy = np.fromfile('gl-latlong-1km-landcover.bsq', shape = (43200,21600), dtype = np.int8)
map_numpy = np.memmap('gl-latlong-1km-landcover.bsq', shape = (21600,43200,), dtype = np.int8)

maryland_colors = np.array([( 68,  79, 137),
                                (  1, 100,   0),
                                (  1, 130,   0),
                                (151, 191,  71),
                                (  2, 220,   0),
                                (  0, 255,   0),
                                (146, 174,  47),
                                (220, 206,   0),
                                (255, 173,   0),
                                (255, 251, 195),
                                (140,  72,   9),
                                (247, 165, 255),
                                (255, 199, 174),
                                (  0, 255, 255),]) / 255
maryland_cmap = colors.ListedColormap(maryland_colors)

figure =  plt.figure()
map_plot = plt.imshow(map_numpy[::50,::50], cmap= maryland_cmap)

x= 71 #WEST/EAST
y= 53 #NORD/SOUTH

def is_this_land(x:float, y:float, map_transform):
    
    TRANSF_CONST = 180/map_transform.shape[0] 
    x_new = (x + 180)/TRANSF_CONST
    y_new = (90 - y)/TRANSF_CONST
    try:
        ans = map_transform[int(y_new), int(x_new)] > 0
        return ans, int(x_new), y_new, TRANSF_CONST,  map_transform[int(y_new), int(x_new)]
    except:
        print("coordinates out of limit")
        pass
    


print(is_this_land(x,y,map_numpy))    




#plt.show()

#erq_file = np.loadtxt('events_4.5(1).txt', delimiter = ";", dtype = str, encoding= 'latin1')

#print(erq_file[1,:])